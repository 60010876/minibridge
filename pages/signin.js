import React from 'react'
import Signin from '../components/Signin/signinPage'
import Layout from '../components/Layout/layout'

export default function HomePage() {
  return(
    <Layout >
      <Signin />
    </Layout>
  )
}