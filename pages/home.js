import React from 'react'
import Home from '../components/Home'
import Layout from '../components/Layout/layout'

export default function HomePage() {
  return (
    <Layout >
      <Home />
    </Layout>
  )
}