import React from 'react'
import Signup from '../components/Signup/signupPage'
import Layout from '../components/Layout/layout'

export default function HomePage() {
  return(
    <Layout >
      <Signup />
    </Layout>
  )
}