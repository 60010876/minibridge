import HowtoplayPage from '../components/Howtoplay/howtoplay'
import Layout from '../components/Layout/layout'

export default function Howtoplay() {
    return (
        <Layout >
            <HowtoplayPage />
        </Layout>
    )
}
