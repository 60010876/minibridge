import React, { Fragment, useState } from 'react'
import { Button, InputItem, Flex, WhiteSpace } from 'antd-mobile';
import Router from 'next/router'

const Signin = () => {
    const handleButtonClick = (page) => {
        Router.push('/' + page)
    }

    return (
        <Fragment>
            <div className="signinPage">
                <img src="./lada.jpg" />
                <WhiteSpace/>
                Username or email
                <InputItem className="login-input"></InputItem>                
                Password
                <InputItem className="login-input"></InputItem>
                <div className="text-test">
                ลืมรหัสผ่าน  ? 
                </div>
                <WhiteSpace/>
                <WhiteSpace/>
                <Button className="" onClick={() => handleButtonClick("signincomplete")}>Login</Button>
                <WhiteSpace/>
                <WhiteSpace/>
                <Button className="" onClick={() => handleButtonClick("signup")}>sign up</Button>
            </div> 
        </Fragment> 
    )
}

export default Signin