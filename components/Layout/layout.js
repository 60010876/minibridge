import React, { Fragment } from 'react'
import { useRouter } from 'next/router'
import Head from 'next/head'
import HeadBadge from '../Header/HeadBadge'
const Layout = props => {
  const url = useRouter().pathname
  const isBack = url !== '/'
  return (
    <Fragment>
      <Head>
        <title>MiniBridge</title>
        <meta name='viewport' 
     content='width=device-width, initial-scale=1.0, maximum-scale=1.0, 
     user-scalable=0' />
        <meta charSet='utf-8' />
      </Head>

      <div className='app-container'>
        <HeadBadge />
        <div className='main-container'>
          {props.children}
        </div>

      </div>
    </Fragment>
  )
}

export default Layout;



