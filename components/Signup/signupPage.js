import React, { Fragment, useState } from 'react'
import { Pagination, Icon, Flex, InputItem } from 'antd-mobile';
import Router from 'next/router';

const Signup = () => {
    const [page, setPage] = useState(1)
    const [step, setStep] = useState(0)
    const clickNextStep = () => {
        setStep(prevActiveStep => prevActiveStep + 1)
        console.log(step)
    }

    const clickNextPage = () => {
        setPage(page != 3 ? page + 1 : 3)
    }
    const clickPrePage = () => {
        setPage(page != 1 ? page - 1 : 1)
    }
    const renderMain = () => {
        switch (step) {
            default:
            case 0: return renderFirstStep()
            case 1: return renderSecondStep()
        }
    }
    const renderFirstStep = () => {
        return (
            <Fragment>
                <div className="coiner">
                    {page == 1 && (<img src="/signuppage/PLAYING_CARDS.svg" className="img-signup mb-3" />)
                    }
                    {page == 2 && (<img src="/signuppage/CHAMPIONS.svg" className="img-signup mb-3" />)
                    }
                    {page == 3 && (<img src="/signuppage/3.png" className="img-signup mb-3" />)
                    }
                    <div className="text-bottom-center">
                        <p className="mb-0">ลงทะเบียน</p></div>

                </div>
                <div className="input-container">
                    {page == 1 && (
                        <Fragment >

                            <p className="text-left font-weight-bold"> Username </p>
                            <input type="text" class="form-control mb-3" placeholder="Username" aria-label="Username" />
                            <p className="text-left font-weight-bold"> Email</p>
                            <input type="email" class="form-control mb-3" placeholder="Email" aria-label="Email" />

                            <p > ฉันมีบัญชีผู้ใช้แล้วต้องการ   <p className=" text-primary"onClick={() => Router.push("/signin")}>เข้าสู่ระบบ</p>  </p>
                        </Fragment>


                    )}
                    {page == 2 && (
                        <Fragment >
                            <p className="text-left font-weight-bold"> ชื่อ </p>
                            <input type="text" class="form-control mb-3" placeholder="ชื่อ" aria-label="firstname" />
                            <p className="text-left font-weight-bold"> นามสกุล </p>
                            <input type="text" class="form-control mb-3" placeholder="นามสกุล" aria-label="lastname" />

                        </Fragment>


                    )}
                    {page == 3 && (
                        <Fragment >
                            <p className="text-left font-weight-bold"> รหัสผ่าน </p>
                            <input type="password" class="form-control mb-3" placeholder="รหัสผ่าน" aria-label="password" />
                            <p className="text-left font-weight-bold"> ยืนยันรหัสผ่าน </p>
                            <input type="password" class="form-control mb-3" placeholder="ยืนยันรหัสผ่าน" aria-label="confirmpassword" />
                            <button type="button" class="btn btn-info" onClick={clickNextStep}>ลงทะเบียน</button>

                        </Fragment>


                    )}
                </div>
                <Flex justify="between" className="pagination-container">
                    {page != 1 ? <Icon type="left" onClick={clickPrePage} /> : <Icon />}
                    <Pagination mode="pointer" total={3} current={page} />
                    {page != 3 ? <Icon type="right" onClick={clickNextPage} /> : <Icon />}

                </Flex>
            </Fragment>
        )
    }
    const renderSecondStep = () => {
        return (
            <Fragment>
                <div className="coiner">
                    <img src="/signuppage/CHAMPIONS.svg" className="img-signup mb-3" />

                    <div className="text-bottom-center">
                        <p className="mb-0">ลงทะเบียนสำเร็จ</p></div>
                </div>
                <div className="input-container ">
                    ลงทะเบียนสำเร็จ คุณสามารถเข้าสู่ระบบ<br />
                    เพื่อสร้าง Tournament หรือสมัครเข้า<br />
                    ร่วมการแข่ง Tournament ที่คุณสนใจ                
                    <button type="button" class="btn btn-info mt-5" onClick={()=>Router.push("/singin")}>เข้าสู่ระบบ</button>

                </div>


            </Fragment>


        )
    }

    return (
        <Fragment>
            <div className="signup-page">
                {renderMain()}
            </div>

        </Fragment>
    )

}

export default Signup