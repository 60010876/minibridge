import React, { Fragment } from 'react'
import Router from 'next/router'
import Carousel from 'nuka-carousel';

const Home = () => {
  const handleButtonClick = (page) => {
    Router.push('/' + page)
  }
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <Fragment>
      <div className="home-page">

        <Carousel className="carousel" >
          <img src="/signuppage/PLAYING_CARDS.svg" />
          <img src="/signuppage/CHAMPIONS.svg" />
          <img src="/signuppage/3.png" />

        </Carousel>
        <div className="group-buttom">
          <button type="button" className="btn btn-info mb-3 " onClick={() => handleButtonClick("signup")}>ลงทะเบียน</button><br />
          <button type="button" className="btn btn-info  mb-3" onClick={() => handleButtonClick("signin")}>เข้าสู่ระบบ</button><br />
          <button type="button" className="btn btn-info  mb-3" onClick={() => handleButtonClick("howtoplay")}>วิธีเล่นไพ่บริดจ์</button>

        </div>

      </div>
    </Fragment>
  )
}

export default Home