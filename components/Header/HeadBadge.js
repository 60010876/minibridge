import React, { useState, Fragment } from 'react'
import { NavBar, Drawer, List, Icon } from 'antd-mobile';
import { useRouter } from 'next/router'
import { FaChevronLeft, FaBars } from 'react-icons/fa';
import Router from 'next/router'

const HeadBadge = ({ isBack }) => {

  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(!visible);
  };

  const router = useRouter()

  const handleBackClick = () => {
    router.back()
  }
  const sidebar = (<List > </List>);
  return (
    <Fragment>
      <NavBar
        className="nav-top"
        mode="light"
        leftContent={
          [
            (router.pathname != "/" && <Icon key="1" type="left" onClick={handleBackClick} />)
          ]
        }
        rightContent={
          [
            <Icon key="1" type="ellipsis" onClick={() => setVisible(!visible)} />

          ]
        }
      >
        {router.pathname}
      </NavBar>
      {/* <Drawer
        className="my-drawer"
        title="Drawer"
        position="right"
        sidebar={sidebar}
        open={visible}
        onOpenChange={showDrawer}
      /> */}

    </Fragment>


  )
}

export default HeadBadge